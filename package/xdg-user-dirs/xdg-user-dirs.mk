################################################################################
#
# xdg-user-dirs
#
################################################################################

XDG_USER_DIRS_VERSION = 0.18
XDG_USER_DIRS_SOURCE = xdg-user-dirs-$(XDG_USER_DIRS_VERSION).tar.gz
XDG_USER_DIRS_SITE = http://user-dirs.freedesktop.org/releases
XDG_USER_DIRS_LICENSE = GPL-2.0
XDG_USER_DIRS_LICENSE_FILES = COPYING
XDG_USER_DIRS_INSTALL_STAGING = YES
XDG_USER_DIRS_INSTALL_DEPENDENCIES = \
	libxslt \
	$(TARGET_NLS_DEPENDENCIES)

$(eval $(autotools-package))
